#include <iostream>
using namespace std;

int main()
{
    int myArray[6] = {10, 22, 37, 55, 92, 118};
    int kiri = 0;
    int kanan = sizeof(myArray)/sizeof(myArray[0])-1;
    int tengah = 0;
    bool cari = false;
    int masukan;

    // Masukan data yang dicari
    cout << "Input data : ";
    cin >> masukan;

    // Proses pencarian bagi dua
    while ((cari == false) && (kanan >= kiri)){
        tengah = (kanan + kiri) / 2;
        
        if (myArray[tengah] == masukan){
            cari = true;
        } else if (myArray[tengah] > masukan){
                kanan -= 1;
            } else {
                kiri += 1;
            }
    }

    // Keluaran hasil pencarian
    if (cari){
        cout << "Data ketemu di indeks : " << tengah << endl;
    } else{
            cout << "Data tidak ditemukan!" << endl;
        }

    // Mengembalikan nilai
    return 0;
}